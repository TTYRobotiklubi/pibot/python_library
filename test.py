from commRaspMain import PiBot
import time

mybot = PiBot()

if not mybot._adc_conf():
	print('bad ADC conf')
if not mybot._motors_enable():
	print('bad motor conf')
if not mybot._servo_enable():
	print('bad servo conf')
if not mybot._encoders_enable():
	print('bad enc conf')
if not mybot._imu_enable():
	print('bad imu conf')
motor = 0
servo = 0
buzzer = 0

motorBase = 50

lineset = 600

while True:
	print(int(round(time.time() * 1000)))
	mybot._imu_read()
	print(int(round(time.time() * 1000)))
	mybot._adc_read()
	print(int(round(time.time() * 1000)))
	mybot._encoders_get()
	print(int(round(time.time() * 1000)))
	print(mybot.imu)

	print("--------------------")

	print("MAZE RIGT: " + str(mybot.sensor[0]))
	print("MAZE RDAG: " + str(mybot.sensor[1]))
	print("MAZE RFRT: " + str(mybot.sensor[2]))

	print("MAZE LEFT: " + str(mybot.sensor[5]))
	print("MAZE LDAG: " + str(mybot.sensor[4]))
	print("MAZE LFRT: " + str(mybot.sensor[3]))

	print("SHRP LEFT: " + str(mybot.sensor[6]))
	print("SHRP FRNT: " + str(mybot.sensor[7]))
	print("SHRP RIGT: " + str(mybot.sensor[14]))

	print("LINE RIGT: " + str(mybot.sensor[8:11]))
	print("LINE LEFT: " + str(mybot.sensor[11:14]))

	print("ENCODERS: " + str(mybot.encoder))

	#time.sleep(4)
	
	lineCount=0
	error=0
	i = 0 
	while i < 6:
		if mybot.sensor[8+i] < lineset:
			error += i
			lineCount += 1
		i +=1
	if error != 0:
		error = (error/lineCount)-2.5
		oldError = error
	else:
		if oldError > 0:
			error = motorBase/8
		else:
			error = -motorBase/8
	
	if error > 0:
		mybot._motorR_set(motorBase)
		mybot._motorL_set(motorBase - (error*8))
	else:
		mybot._motorR_set(motorBase + (error*8))
		mybot._motorL_set(motorBase)

